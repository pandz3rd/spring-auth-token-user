# Verification user token

* Clone: https://gitlab.com/pandz3rd/spring-auth-token.git
* Login - to get token
* Run this project `mvn spring-boot:run`
* Hit endpoint to get user, using token from `spring auth token`

