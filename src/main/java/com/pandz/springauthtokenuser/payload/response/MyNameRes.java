package com.pandz.springauthtokenuser.payload.response;

public class MyNameRes {
  private String myName;

  public MyNameRes(String myName) { this.myName = myName; }

  public String getMyName() {
    return myName;
  }

  public void setMyName(String myName) {
    this.myName = myName;
  }
}
