package com.pandz.springauthtokenuser.payload.response;

import com.pandz.springauthtoken.model.User;

import java.util.List;

public class AllUserRes {
  private List<User> users;

  public AllUserRes(List<User> users) {
    this.users = users;
  }

  public AllUserRes() {}

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }
}
