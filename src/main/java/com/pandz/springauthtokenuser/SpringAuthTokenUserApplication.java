package com.pandz.springauthtokenuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.pandz")
public class SpringAuthTokenUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAuthTokenUserApplication.class, args);
	}

}
