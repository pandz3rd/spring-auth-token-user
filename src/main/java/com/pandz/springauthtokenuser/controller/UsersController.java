package com.pandz.springauthtokenuser.controller;

import com.pandz.springauthtoken.model.User;
import com.pandz.springauthtoken.payload.response.MessageRes;
import com.pandz.springauthtoken.repository.UserRepository;
import com.pandz.springauthtokenuser.payload.response.AllUserRes;
import com.pandz.springauthtokenuser.payload.response.MyNameRes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/api/v1")
public class UsersController {
  private static final Logger log = LoggerFactory.getLogger(UsersController.class);

  @Autowired
  UserRepository userRepository;

  @GetMapping("/user")
//  @PreAuthorize("hasRole('ADMIN')") // only admin can access this
  public ResponseEntity<?> getUsers(Principal principal, @RequestParam(value = "username", required = false) String username) {
    log.info("### Start find all users");
    log.info("Username yang login: " + principal.getName());
    log.info("Username yang di search: " + (!username.isEmpty() ? username : "all user"));

    List<User> resultUser = new ArrayList<>();

    if (username.isEmpty()) {
      resultUser = userRepository.findAll();
    } else {
      User user = userRepository.findByUsername(username);

      if (user == null) {
        return ResponseEntity.badRequest()
          .body(new MessageRes("User with username: " + username + " not found."));
      }

      resultUser.add(user);
    }

    return ResponseEntity.ok(new AllUserRes(resultUser));
  }

  @GetMapping("/me")
  public ResponseEntity<?> getMyName(Principal principal) {
    String myName = principal.getName();

    return ResponseEntity.ok(new MyNameRes(myName));
  }

}
